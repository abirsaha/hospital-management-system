@can('isAdmin')
<!DOCTYPE html>
<html>
<head>
    <title>Multiple data send</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js">
</script>
</head>
<body>
<div class="container">
 <br>
 @if(Session::has('success'))
 <div class="alert alert-success">
     {{Session::get('success')}}
 </div>
 @endif
 
     <form  method="POST">
         {{csrf_field()}}
         <section>
             <div class="panel panel-header">

                 <div class="row">
                     <div class="col-md-6">
                 <div class="form-group">
                     <input type="text" name="customer_name" class="form-control" placeholder=" enter your company name">
                 </div></div>
                 <div class="col-md-6">
                 <div class="form-group">
                     <input type="date" name="customer_address" class="form-control" >
                 </div></div>

             </div></div>
             <div class="panel panel-footer" >
                 <table class="table table-bordered">
                     <thead>
                         <tr>
                             <th>Medicine Name</th>
                             
                             <th>Quantity</th>
                             
                             <th><a href="#" class="addRow"><i class="glyphicon glyphicon-plus"></i></a></th>
                         </tr>
                     </thead>
                     <tbody>
         <tr>
         <td><select name="medicine_name[]" class="form-control">
                            <option>select one</option>
                    @foreach($paients as $paients)
                    <option value="{{$paients->medicine_name}}">{{$paients->medicine_name}}</option>
                    @endforeach
                        </select>


                    </td>
            


                    
                        
            
       
           <td><input type="text" name="quantity[]" class="form-control quantity" required=""></td>
           
           
         <td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a></td>
         </tr>
                         </tr>
                     </tbody>
                     <tfoot>
                         <tr>
                             <td style="border: none"></td>
                             
                             <td><b class="total"></b> </td>
                             <td><input type="submit" name="" value="Submit" class="btn btn-success"></td>
                         </tr>
                     </tfoot>
                 </table>
             </div>
         </section>
     </form>
</div>
<script type="text/javascript">
 
    $('.addRow').on('click',function(){
        addRow();
    });
   function addRow()
    {
        var tr='<tr>'+
        '<td><input type="text" name="medicine_name[]" class="form-control" required=""></td>'+
        

        '<td><input type="text" name="quantity[]" class="form-control quantity" required=""></td>'+
        

        '<td><a href="#" class="btn btn-danger remove"><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '</tr>';
        $('tbody').append(tr);
    };
    $('.remove').live('click',function(){
        var last=$('tbody tr').length;
        if(last==1){
            alert("you can not remove last row");
        }
        else{
             $(this).parent().parent().remove();
        }
     
    });

</script>
</body>
</html>
@endcan








