<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('fontEnd.master');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard','HomeController@dashboard');
//paients

Route::get('/addpaient','HomeController@addpaient');

Route::post('/addpaient','paientController@store');


Route::get('/showpaients','paientController@show');

Route::get('/updatepaient{id}','paientController@editpaient');

Route::post('/updatepaient{id}','paientController@updatepaient');

Route::get('/deletepaient{id}','paientController@deletepaient');

Route::get('/searchpaient','HomeController@searchpaient');

Route::post('/searchpaient','paientController@searchpaient');

//users

Route::get('/showusers','userController@showuser');

Route::get('/updateuser{id}','userController@show');

Route::post('/updateuser{id}','userController@updateuser');

Route::get('/deleteuser{id}','userController@deleteuser');

//stuffs

Route::get('/addstuffs','HomeController@addstuff');

Route::post('/addstuffs','stuffController@store');

Route::get('/showstuff','stuffController@showstuff');


// Route::get('updatestuff{stuff_id}','stuffController@updatestuff');


//medicine

Route::get('/addmedicine','HomeController@addmedicine');

Route::post('/addmedicine','stuffController@storemedicine');

Route::get('/addmedicine2','stuffController@addmedicine2');

Route::get('/showmedicine','medicineController@showmedicine');

Route::get('/updatemedicine{id}','medicineController@updatemedicine1');

Route::post('/updatemedicine{id}','medicineController@updatemedicine2');

Route::get('/stockofmedicine','medicineController@stock');

Route::post('/stockofmedicine','medicineController@stockresult');

Route::get('/buymedicinefromcompany','buymedicineforc@index');

Route::post('/buymedicinefromcompany','buymedicineforc@store');

Route::get('/sellmedicine','HomeController@sellmedicine');

Route::post('/sellmedicine','medicineController@sellmedicine1');

Route::get('/newsellmedicine','HomeController@sellmedicine2');

Route::post('/newsellmedicine','HomeController@sellmedicine3');




Route::get('/adddoctor','HomeController@adddoctor');

Route::post('/adddoctor','doctorController@store');

Route::get('/showdoctor','doctorController@showdoctor'); 

Route::get('/updatedoctor{d_id}','doctorController@editdoctor');

Route::post('/updatedoctor{d_id}','doctorController@updatedoctor');

Route::get('/searchdoctor','doctorController@searchdoctor');

Route::post('/searchdoctor','doctorController@resultsearchdoctor');

Route::get('/docfile{d_id}','doctorController@docfile');


//cabin

Route::get('/addcabin','HomeController@addcabin');

Route::post('/addcabin','cabinController@store');

Route::get('/showcabin','cabinController@show');

Route::get('/freecabin','HomeController@freecabin');

Route::get('/searchbycabontype','HomeController@searchcabin');

Route::post('/searchbycabontype','cabinController@searchcabin');


//admit

Route::get('/admit','HomeController@admitted');

Route::post('/admit','admitController@store');


Route::get('/showadmitrecord','admitController@searchadmit');

Route::post('/showadmitrecord','admitController@searchadmit1');



 Route::get('/searchadmitreport','HomeController@searchadmit');

 Route::post('/searchadmitreport','HomeController@searchadmited');

Route::get('/assigntodoctor','HomeController@assigntodoctor');

Route::post('/assigntodoctor','admitController@assigntodoctor'); //donot route in the blade file


//payment

Route::get('/addpayment','HomeController@addpayment');

Route::get('/checkhospitalpayment','HomeController@hospitalpayment1'); 

Route::post('/checkhospitalpayment','paymentController@hospitalpayment');   

Route::get('/hospitalpaymentform','HomeController@hospitalpayment2');

Route::post('/hospitalpaymentform','paymentController@hospitalpayment3');




Route::get('/checkmedicinepayment','HomeController@medicinepayment1');//donot route in the blade file

Route::post('/checkmedicinepayment','paymentController@medicinepayment2');//donot route in the blade file

Route::get('/medicinepaymentform','HomeController@medicinepayment3');

Route::post('/medicinepaymentform','paymentController@medicinepayment4');

Route::get('/customersmedicine','HomeController@customermedicine');

Route::post('/customersmedicine','paymentController@customermedicines');


//operations

Route::get('addoperation','HomeController@addoperation');

Route::post('addoperation','operationController@store');

//relase 

Route::get('/addrelase','HomeController@addrelase1');

Route::post('/addrelase','paymentController@addrelase2');

Route::post('/searchbycabincost','paymentController@cabincost');

Route::get('/addprescription','HomeController@addprescription');

Route::post('/addprescription','doctorController@addprescription');

Route::get('/pre_med','HomeController@addpre_med');

Route::post('/pre_med','premediController@store');


