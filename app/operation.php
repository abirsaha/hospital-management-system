<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class operation extends Model
{
    protected $fillable = [
        'p_id','d_id','o_date','o_cost','purpose',
    ];
}
