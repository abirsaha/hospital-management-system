<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\doctor;
use App\prescription;
use App\paient;
use Gate;
use phpword;
use PDF;



class doctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) && (!Gate::allows('ispaient'))  ){
         abort(404,"Sorry you can not do this action");
         }
         else{
           
            DB::table('doctors')->insert([
            'd_name'=>$request->d_name,
            'd_email'=>$request->d_email,
            'd_age'=>$request->d_age,
            'd_salary'=>$request->d_salary,
            'd_address' => $request->d_address,
            'd_phone'=>$request->d_phone,
            'd_room_num'=>$request->d_room_num,
            'speacilist'=>$request->speacilist,
            'd_password' =>bcrypt($request->d_password),
            

            
        ]);



           DB::table('users')->insert([
            'name'=>$request->d_name,
            'email'=>$request->d_email,
            'password' =>bcrypt($request->d_password),
            'u_type' => "doctor",

            
        ]);

return redirect('/adddoctor');



// $phpWord = new \PhpOffice\PhpWord\PhpWord();
//         $section = $phpWord->addSection();
//         $section->addImage("./images/Krunal.jpg"); 
//         $text = $section->addText('name : '.$request->get('d_name'));
//         $text = $section->addText($request->get('d_email'));
        
//         $text = $section->addText($request->get('d_phone'),array('name'=>'Arial','size' => 20,'bold' => true));
          
//         $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
//         $objWriter->save('Appdividend.html');
//         return response()->download(public_path('Appdividend.html'));
            


 //  $phpWord1 = new \PhpOffice\PhpWord\PhpWord();
 //        $section = $phpWord1->addSection();
 //        $text = $section->addText($request->get('d_name'));
 //        $text = $section->addText($request->get('d_email'));
 //        $text = $section->addText($request->get('d_phone'),array('name'=>'Arial','size' => 20,'bold' => true));
 //        $section->addImage("./images/Krunal.jpg");  
 //        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord1, 'PDF');
 //        $objWriter->save('Appdividend.pdf');
 //        return response()->download(public_path('Appdividend.pdf'));




 // $phpWord2 = new \PhpOffice\PhpWord\PhpWord();
 //        $section = $phpWord2->addSection();
 //        $text = $section->addText($request->get('d_name'));
 //        $text = $section->addText($request->get('d_email'));
 //        $text = $section->addText($request->get('d_phone'),array('name'=>'Arial','size' => 20,'bold' => true));
 //        $section->addImage("./images/Krunal.jpg");  
 //        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord2, 'ODText');
 //        $objWriter->save('Appdividend.odt');
 //        return response()->download(public_path('Appdividend.odt'));









 // $data = ['title' => 'Welcome to HDTuto.com'];
 //        $pdf = PDF::loadView('myPDF', $data);
  
 //        return $pdf->download('itsolutionstuff.pdf');


      




    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showdoctor(){
        $doctor=doctor::all();
        return view('fontEnd.shows.showdoctor',['doctor'=>$doctor]);
    }

    public function searchdoctor(){
          if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

        else{

            return view('fontEnd.searchingforms.searchdoctor');

        }
    }


    public function resultsearchdoctor(Request $request){
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

        else{

            $searchdoctor = DB::table('doctors')
        
            ->Where('d_name', 'like', '%'.$request->searching.'%')
            ->orWhere('id',$request->searching)
            ->orWhere('d_phone','like','%'.$request->searching.'%')
            ->orWhere('d_email','like','%'.$request->searching.'%')
            ->orWhere('speacilist','like','%'.$request->searching.'%')
         ->select('*')
         ->get();
         return view('fontEnd.shows.searcheddoctor',['searchdoctor'=>$searchdoctor]);
        }
    }

    


    public function editdoctor($d_id){
        //echo $id;
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        $doctors=doctor::where('id',$d_id)->first();
        return view('fontEnd.updateforms.updatedoctor',['doctors'=>$doctors]);
       // echo " <pre>";
       // print_r($doctors);
    }


    public function docfile($d_id){
        //echo $id;
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        else{

            $results=DB::table('doctors')
            ->where('id',$d_id)
            ->get();
        
        foreach ($results as $results) {
            
        
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $section->addImage("./images/Krunal.jpg"); 
        
        $text = $section->addText('Doctor name : ' .$results->d_name);
        $text = $section->addText('Doctor email : ' .$results->d_email);
        $text = $section->addText('Doctor phone: ' .$results->d_phone);
        $text = $section->addText('Doctors speciality : ' .$results->speacilist);
        $text = $section->addText(' Doctors room number :  '.$results->d_room_num);
       
        
      
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
        // $docfilename=$request->d_name;
         $objWriter->save($results->d_name);

        return response()->download(public_path($results->d_name));

    }


        // return view('fontEnd.updateforms.updatedoctor',['doctors'=>$doctors]);
        

       // echo " <pre>";
       // print_r($doctors);
    }
    }




    public function updatedoctor(Request $request){
       // dd($request->all());
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        else{
        $doctors=doctor::find($request->id);
        $doctor = $doctors->d_email;


        $doctors->d_name=$request->d_name;
        $doctors->d_email=$request->d_email;
        
        $doctors->d_age=$request->d_age;
        $doctors->d_salary=$request->d_salary;
        $doctors->d_address=$request->d_address;
        $doctors->d_phone=$request->d_phone;
        $doctors->d_room_num=$request->d_room_num;
        // $doctors->speacilist=$request->speacilist;
        $doctors->save();

        

        $users=DB::table('users')
          ->where('email',$doctor)
          ->update(
        ['email' => $request->d_email, 'name' => $request->d_name],
        ['password' =>bcrypt($request->d_phone) ]
    );


        return redirect('/showdoctor');



    // DB::table('doctors')
    //         ->where('d_id', $request->d_id)
    //         ->update(['d_name' => $request->d_name])
    //         ->update(['d_email' => $request->d_email])
    //         ->update(['d_age'=>$request->d_age])
    //         ->update(['d_salary'=>$request->d_salary])
    //         ->update(['d_address'=>$request->d_address])
    //         ->update(['d_phone'=>$request->d_phone])
    //         ->update(['d_room_num'=>$request->d_room_num])
    //         ->update(['speacilist'=>$request->speacilist]);


    // return redirect('/showdoctor');


    }}

    public function addprescription(Request $request){
        prescription::create($request->all());
        return redirect('/addprescription');
    }
    
    

}
