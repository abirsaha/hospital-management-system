<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\medicine;
use App\sellmedicine;
use DB;

class medicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showmedicine()
    {
        //
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{

            $medicines = medicine::all();
            return view('fontEnd.shows.showmedicines',['medicines'=>$medicines]);

         }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updatemedicine1($id){
 

  if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

        
else{

        $medicine=DB::table('medicines')
        
           
            ->Where('m_id',$id)
            
         ->select('*')
         ->get();
        return view('fontEnd.updateforms.updatemedicine',['medicine'=>$medicine]);
    }
}

      public function updatemedicine2(Request $request){
       // dd($request->all());
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

        else{
            $medicine = DB::table('medicines')
        
            ->Where('m_id', $request->m_id)
            //  ->update(['medicine_name' =>$request->medicine_name])
            // ->update(['medicine_catagory' =>$request->medicine_catagory]);


         //   opertyAdvert::where('id', $id)->where('user_id', Auth::id()
            ->update([
        "medicine_name"       => $request->medicine_name,
        "medicine_catagory"     => $request->medicine_catagory,
        "sellprice"      => $request->sellprice,
        "buyprice"        => $request->buyprice,
       
      ]);

      return redirect('/showmedicine');




        
        //     $paients=paient::find($request->id);
        // $paients->p_name=$request->p_name;
        // $paients->p_age=$request->p_age;
        // $paients->p_email=$request->p_email;
        // $paients->save();
        // return redirect('/showpaients');
          //   echo "<pre>";
          // print_r($medicine);
        }
        

    }

    public function stock(){
           if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        else{
            $medicine = medicine::all()->unique('company_name');
            
            return view('fontEnd.searchingforms.stockmedicine',['medicine'=>$medicine]);
        }
    }


    public function stockresult(Request $request){
           if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

        else{
            $medicine = DB::table('medicines')
            ->Where('company_name',$request->company_name)
            ->where('quantity','<',10)
            ->select('*')
            ->get();
          //     echo "<pre>";
          // print_r($medicine);

            return view('fontEnd.shows.searchedstock',['medicine'=>$medicine]);
           

        }

    }


     


    public function sellmedicine1(Request $request)
    {
        
        if($request->quantity < 0){
           
           echo "<script>window.alert('Quantity cannot be negative')</script>";
      
          exit;
        

        }
$sellingprice =  DB::table('medicines')->where('m_id', $request->m_id)->select('sellprice')->get();

  foreach($sellingprice as $sellingprice)
              $total = $sellingprice->sellprice * $request->quantity ;
         

$quantity =  DB::table('medicines')->where('m_id', $request->m_id)->select('quantity')->get();

  foreach($quantity as $quantity)
              $new_quantity = $quantity->quantity - $request->quantity;
            
            if($new_quantity<0){
                  echo "<script>window.alert('Medicine stock crossed')</script>";
                  exit();
            }

             

      
        DB::table('sellmedicines')->insert([
            'phone'=>$request->phone,
            'm_id'=>$request->m_id,
            'quantity'=>$request->quantity,
            'total'=>$total,
            'sell_date'=>$request->sell_date,
            

            
        ]);

        ( DB::table('medicines')
    ->where('m_id', $request->m_id)
    
    ->update(['quantity' => DB::raw('quantity - ' .         
      $request->quantity)]));


  

        return redirect('/sellmedicine');

    }





    

}
