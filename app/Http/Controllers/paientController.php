<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\paient;
use DB;
use Gate;


class paientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{
           
        paient::create($request->all());
           DB::table('users')->insert([
            'name'=>$request->p_name,
            'email'=>$request->p_email,
            'password' =>bcrypt($request->p_phone),
            'u_type' => "user",
            
        ]);
        return redirect('/addpaient');
}
          
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     
  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function show(){

        $paients=paient::all();
        return view('fontEnd.shows.showpaient',['paients'=>$paients]);
    }

    public function editpaient($id){
        //echo $id;
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        $paients=paient::where('id',$id)->first();
        return view('fontEnd.updateforms.updatepaient',['paients'=>$paients]);
    }


    public function updatepaient(Request $request){
       // dd($request->all());
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        else{
        $paients=paient::find($request->id);
        $paient = $paients->p_email;

        $paients->p_name=$request->p_name;
        $paients->p_age=$request->p_age;
        $paients->p_email=$request->p_email;
        $paient_email = $paients->p_email;
        $paient_phone = $paients->p_phone;


        $paients->save();
       

         $users=DB::table('users')
          ->where('email',$paient)
          ->update(
        ['email' => $request->p_email, 'name' => $request->p_name],
        ['password' =>bcrypt($paients->p_phone) ]);
          return redirect('/showpaients');
        }
    }

    public function deletepaient(Request $request){
        $paients=paient::find($request->id);
        $paients->delete();
        return redirect('/showpaients');
    }




    public function dashboard(){

       if(!Gate::allows('isAdmin')){
         abort(404,"Sorry you can not do this action");
        }


        if(!Gate::allows('isUser')){
         abort(404,"Sorry you can not do this action");
        }

        if(!Gate::allows('isDoctor')){
         abort(404,"Sorry you can not do this action");
        }

        if(!Gate::allows('isUser')){
         abort(404,"Sorry you can not do this action");
        }
        return view('fontEnd.forms.dashboard');
    }

     public function searchpaient(Request $request){
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

        $paients=DB::table('paients')
        
            ->Where('p_name', 'like', '%'.$request->p_name.'%')
            ->orWhere('id',$request->p_name)
            ->orWhere('p_phone','like','%'.$request->p_name.'%')
            ->select('*')
            ->get();
          // echo "<pre>";
          // print_r($players);
        return view('fontEnd.shows.searchedpaient',['paients'=>$paients]);



    }


}
