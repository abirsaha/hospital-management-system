<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stuff;
use App\medicine;
use DB;
use Gate;
class stuffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
        stuff::create($request->all());
        
           DB::table('users')->insert([
            'name'=>$request->s_name,
            'email'=>$request->s_email,
            'password' =>bcrypt($request->s_password),
            'u_type' => "stuff",
            
        ]);


           
            return redirect('/addstuffs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showstuff(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         $stuffs = stuff::all();
         return view('fontEnd.shows.showstuff',['stuffs'=>$stuffs]);

          $paients=paient::all();
        return view('fontEnd.shows.showpaient',['paients'=>$paients]);
    }

    

    public function storemedicine(Request $request){

        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
       
        if(count($request->medicine_name) > 0)
        {
        foreach($request->medicine_name as $item=>$v){
            $data2=array(
                
                'medicine_name'=>$request->medicine_name[$item],
                'medicine_catagory'=>$request->medicine_catagory[$item],
                'buyprice'=>$request->buyprice[$item],
                'quantity'=>$request->quantity[$item],
                'sellprice'=>$request->sellprice[$item],
                'company_name'=>$request->customer_name,
                'buyingdate'=>$request->customer_address
            );
            
        medicine::insert($data2);
      }
        }
        return redirect()->back()->with('success','data insert successfully');
}


    public function addmedicine2(){
        return view('fontEnd.forms.medicine2');
    }

    public function buymedicinefromcompany(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{

         $paients=medicine::all();
        return view('fontEnd.forms.buymedicinefromcompany',['paients'=>$paients]);
}
    }



    public function updatemedicine(Request $request){

        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
       
        if(count($request->medicine_name) > 0)
        {
        foreach($request->medicine_name as $item=>$v){
            $data2=array(
                
                'medicine_name'=>$request->medicine_name[$item],
                'quantity'=>$request->quantity[$item],
                'company_name'=>$request->customer_name,
                'buyingdate'=>$request->customer_address,

                ( DB::table('medicines')
    ->where('medicine_name', $request->medicine_name[$item])
    
    ->update(['quantity' => DB::raw('quantity + ' .         
      $request->quantity[$item])]))

            );
            
        // medicine::insert($data2);
            // DB::table('medicines')
            // ->where('m_id', $m_id)
            // ->update(['options->enabled' => true]);
 

      }
        }
        return redirect()->back()->with('success','data insert successfully'.$request->m_id[$item]);
}


    }

