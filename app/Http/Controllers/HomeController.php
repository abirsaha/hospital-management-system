<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\paient;
use App\cabin;
use App\doctor;
use App\admit;
use App\medicine;
use App\prescription;
use phpword;
use PDF;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');


        }

    

    public function addpaient(){


    if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }

         
        return view('fontEnd.forms.addpaient');
    }

   


    public function dashboard(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

       return view('fontEnd.forms.dashboard'); 

    }



    public function searchpaient(Request $request){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }

       return view('fontEnd.searchingforms.searchpaient'); 

    }

    public function addstuff(){
      if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }
         else{
         
        return view('fontEnd.forms.addstuffs');
}

    }

    public function addmedicine(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }
         else{
        return view('fontEnd.forms.medicine');
    }
    }


    public function adddoctor(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{
        return view('fontEnd.forms.adddoctor');
    }
    }

    public function addcabin(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }
         else{
        return view('fontEnd.forms.addcabin');
    }
    }

     public function freecabin(){
       if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }
         else{
        $cabins=DB::table('cabins')
        
            ->Where('free_seat','>' , 0)
            ->get();
          // echo "<pre>";
          // print_r($cabin);
       return view('fontEnd.shows.freecabin',['cabins'=>$cabins]);
    }}


    public function searchcabin(){
       if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }

         else{  return view ('fontEnd.searchingforms.searchcabin');
    }}


    public function admitted(){
       if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }
         else{

         $cabins=DB::table('cabins')
            ->select("*")
            ->Where('free_seat','>' , 0)
            ->orderBy('c_cost')
            
            ->get();

         
          $doctors= doctor::all(); 
          $paients= paient::all(); 
        return view('fontEnd.forms.admit', ['cabins'=>$cabins] ,['paients'=>$paients]);
        




    }}




     public function searchadmit(){

         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }

         else{
            
            return view('fontEnd.searchingforms.searchadmit');

         }

     }  

     public function assigntodoctor(){

        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }

         else{

            $doctors = doctor::all();
            $admits= admit::all();
            return view('fontEnd.forms.assigndoctor',['doctors'=>$doctors],['admits'=>$admits]);
         }

     }


       public function searchadmited(Request $request){
     
 if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{

         $results = DB::table('admits')
         ->select('*')
         ->join('paients','admits.p_id','paients.id')
         ->join('cabins','admits.cabin_id','cabins.c_id')
         ->join('paienttodoctors','admits.admit_id','paienttodoctors.admit_id')
         ->join('doctors','paienttodoctors.d_id','doctors.id')
         ->where('admits.admit_id',$request->admit_id)
         ->get();
        
          
          // echo "<pre>";
          // print_r($results);
        
       // return view('fontEnd.shows.searchedadmitreport',['results'=>$results]);
        foreach ($results as $results) {
            
        
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $section->addImage("./images/Krunal.jpg"); 
        $text = $section->addText(' Admit id :  '.$results->admit_id);
        $text = $section->addText(' paient name :  '.$results->p_name);
        $text = $section->addText('paient age : ' .$results->p_age);
        $text = $section->addText('paient email : ' .$results->p_email);
        $text = $section->addText('paient p_address : ' .$results->p_address);
        $text = $section->addText('paient phone : ' .$results->p_phone);
        $text = $section->addText('Doctor name : ' .$results->d_name);
        $text = $section->addText('Doctor email : ' .$results->d_email);
        $text = $section->addText('Doctor phone: ' .$results->d_phone);
        $text = $section->addText('Doctors speciality : ' .$results->speacilist);
        $text = $section->addText(' Doctors room number :  '.$results->d_room_num);
        $text = $section->addText('Cabin number  : ' .$results->c_id);
        
        // $text=$section->addText('paient phone num:'.$results->p_phone,array('name'=>'Arial','size' => 20,'bold' => true,'alignment'=>'center'));
        // $text = $section->addText($request->get('d_email'));
        
        // $text = $section->addText($request->get('d_phone'),array('name'=>'Arial','size' => 20,'bold' => true));
          
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
         $objWriter->save('Appdividend.html');
        return response()->download(public_path('Appdividend.html'));}
            


  // $phpWord1 = new \PhpOffice\PhpWord\PhpWord();
  //       $section = $phpWord1->addSection();
  //       $text = $section->addText($request->get('d_name'));
  //       $text = $section->addText($request->get('d_email'));
        $text = $section->addText($request->get('d_phone'),array('name'=>'Arial','size' => 20,'bold' => true));
  //       $section->addImage("./images/Krunal.jpg");  
        // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord1, 'PDF');
        // $objWriter->save('Appdividend.pdf');
        // return response()->download(public_path('Appdividend.pdf'));




 // $phpWord2 = new \PhpOffice\PhpWord\PhpWord();
 //        $section = $phpWord2->addSection();
 //        $text = $section->addText($request->get('d_name'));
 //        $text = $section->addText($request->get('d_email'));
 //        $text = $section->addText($request->get('d_phone'),array('name'=>'Arial','size' => 20,'bold' => true));
 //        $section->addImage("./images/Krunal.jpg");  
 //        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord2, 'ODText');
 //        $objWriter->save('Appdividend.odt');
 //        return response()->download(public_path('Appdividend.odt'));









 // $data = ['title' => 'Welcome to HDTuto.com'];
 //        $pdf = PDF::loadView('myPDF', $data);
  
 //        return $pdf->download('itsolutionstuff.pdf');


      



    }

      }


      public function sellmedicine(){

         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }

         else{

            $medicines=medicine::all();
            $mobiles =  DB::table('tempmobiles')
                    ->select('*')
                     ->where('m_id', 1)
                    ->get();

            return view('fontEnd.shows.sellmedicine',['medicines'=>$medicines],['mobiles'=>$mobiles]);

            
         }


      } 


      public function addpayment(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{

        return view('fontEnd.shows.paymentforwhat');}
      }   

      public function hospitalpayment1(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{


        return view('fontEnd.shows.hospitalpayment');}
      }   

      public function addoperation(){

         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{


        $paients = paient::all();
        $doctors = doctor::all();
        return view('fontEnd.forms.addoperation',['paients'=>$paients],['doctors'=>$doctors]);}
      } 


      public function medicinepayment1(){
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{
        return view('fontEnd.forms.medicinepaymentform');}
      }

      public function medicinepayment3(){
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{
        return view('fontEnd.forms.givemedicinepayment');}
      }

        public function hospitalpayment2(){

         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }

         else{

            $paients = paient::all();
            return view('fontEnd.forms.hospitalpaymentform',['paients'=>$paients]);
            
         }


      }

     public function addrelase1(){
             if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }

         else{

            $paients = paient::all();
            return view('fontEnd.forms.addrelase',['paients'=>$paients]);
            
         }


     }


     public function customermedicine(){

           if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }

         else{


        return view('fontEnd.forms.customersmedicine');}

     }

     public function sellmedicine2(){
   
        // DB::table('tempmobiles')
        //     ->where('m_id', 1)
        //     ->update(['mobilenum'=>$request->m_number]);

        //  return redirect('/payment'); 
        
        return view('fontEnd.forms.mobilenum');
     }

     public function sellmedicine3(Request $request){
   


        DB::table('tempmobiles')
            ->where('m_id', 1)
            ->update(['mobilenumber'=>$request->m_num]);

         return redirect('/sellmedicine'); 
        
       
     }


     public function addprescription(){


        $doctors = doctor::all();
        $paients = paient::all();
        return view('fontEnd.forms.addprescription',['doctors'=>$doctors],['paients'=>$paients]);
     }

     public function addpre_med(){

        $pre_id= prescription::all();
        $med_id=medicine::all();
        return view('fontEnd.forms.addpre_med',['pre_id'=>$pre_id],['med_id'=>$med_id]);


     }




}

