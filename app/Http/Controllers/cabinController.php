<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cabin;
use DB;
use Gate;
class cabinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }

         else{
        cabin::create($request->all());
        //    DB::table('users')->insert([
        //     'name'=>$request->p_name,
        //     'email'=>$request->p_email,
        //     'password' =>bcrypt($request->p_phone),
        //     'u_type' => "doctor",
            
        // ]);
        return redirect('/addcabin');}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{
        $cabins=cabin::all();
        return view('fontEnd.shows.showcabin',['cabins'=>$cabins]);
    }}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchcabin(Request $request){
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor'))){
         abort(404,"Sorry you can not do this action");
         }
         else{


         $cabins=DB::table('cabins')
        
            ->Where('cabin_type' , $request->cabin_types )
            ->get();
          // echo "<pre>";
          // print_r($cabin);
       return view('fontEnd.shows.searchedcabin',['cabins'=>$cabins]);
    }}

   
}
