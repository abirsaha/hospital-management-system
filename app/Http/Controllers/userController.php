<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Gate;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");
         
        }
        $paients=User::where('id',$id)->first();
        return view('fontEnd.updateforms.updateuser',['paients'=>$paients]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function showuser(){
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action");}

         $users = User::all();
        return view('fontEnd.shows.showusers',['users'=>$users]);
    }


    public function updateuser(Request $request){
       // dd($request->all());
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isDoctor')) && (!Gate::allows('isStuff')) && (!Gate::allows('isUser')) ){
         abort(404,"Sorry you can not do this action"); }
       

        $users=User::find($request->id);
        $users->name=$request->name;
        $users->email=$request->email;
        $users->u_type=$request->u_type;
        $users->save();
        return redirect('/showusers');

    }


    public function deleteuser(Request $request){
        $users=User::find($request->id);
        $users->delete();
        return redirect('/showusers');
    }
}
