<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\operation;
use DB;


class operationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) && (!Gate::allows('ispaient'))  ){
         abort(404,"Sorry you can not do this action");
         }
         else{

            operation::create($request->all());
            // return redirect('/addoperation');



            $operation = DB::table('operations')
                        ->select('*')
                        ->join('paients','operations.p_id','paients.id')
                        ->join('doctors','operations.d_id','doctors.id')
                        ->where('operations.d_id',$request->d_id)
                        ->where('operations.p_id',$request->p_id)
                        ->where('o_date',$request->o_date)
                        ->get();
            return view("fontEnd.instantprint.operationrecipt",['operation'=>$operation]);
            



         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
