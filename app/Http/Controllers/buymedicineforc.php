<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\buymedicine;
use App\medicine;
use DB;

class buymedicineforc extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }

         else{
            $buymedicine = buymedicine::all();
            $medicines = medicine::all(); 
        return view('fontEnd.forms.buymedicineforcompany',['buymedicine'=>$buymedicine],['medicines'=>$medicines]);
    
    }}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((!Gate::allows('isAdmin')) && (!Gate::allows('isUser')) && (!Gate::allows('isStuff')) && (!Gate::allows('isDoctor')) ){
         abort(404,"Sorry you can not do this action");
         }
         else{
        if($request->quantity < 0){
           
           echo "<script>window.alert('Quantity cannot be negative')</script>";
      
          exit;
        

        }
$sellingprice =  DB::table('medicines')->where('m_id', $request->m_id)->select('sellprice')->get();
// $total = $sellingprice * $request->quantity;
  foreach($sellingprice as $sellingprice)
             $total = $sellingprice->sellprice * $request->quantity ;
                
      
        
DB::table('buymedicines')->insert([
            'm_id'=>$request->m_id,
            'quantity'=>$request->quantity,
            'total'=>$total,
            'buy_date'=>$request->buy_date,
            

            
        ]);
        ( DB::table('medicines')
    ->where('m_id', $request->m_id)
    
    ->update(['quantity' => DB::raw('quantity + ' .         
      $request->quantity)]));
  

        return redirect('/buymedicinefromcompany');
}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
