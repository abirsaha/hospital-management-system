<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicine extends Model
{
    //
    protected $fillable = [
        'medicine_name','medicine_catagory','quantity','buyprice','sellprice','company_name','buyingdate',
    ];
}
