<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicinepaymentrecord extends Model
{
    //
    protected $fillable = [
        'p_name','p_phone','amount','payment_date',
    ];
}
