<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cabin extends Model
{
    //
    protected $fillable = [
    'cabin_type','c_cost','capacity','free_seat',];
}
