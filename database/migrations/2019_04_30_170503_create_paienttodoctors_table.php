<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaienttodoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paienttodoctors', function (Blueprint $table) {
          $table->increments('assign_id');
            
            $table->integer('admit_id')->references('admit_id')->on('admits') ->onDelete('cascade') ->onUpdate('cascade');
            
            $table->integer('d_id')->references('id')->on('doctors')->onDelete('cascade')->onUpdate('cascade');
           
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paienttodoctors');
    }
}
