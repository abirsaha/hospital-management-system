<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellmedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellmedicines', function (Blueprint $table) {
              $table->increments('sell_id');

            $table->Integer('m_id')->references('m_id')->on('medicines')->onDelete('cascade')->onUpdate('cascade');
            $table->Integer('quantity');
            $table->Double('total');
            $table->String('phone');
            $table->String('sell_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellmedicines');
    }
}
