<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalpaymentrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitalpaymentrecords', function (Blueprint $table) {
            $table->increments('pay_id');
            $table->Integer('p_id')->references('id')->on('paients')->onDelete('cascade')->onUpdate('cascade');
            $table->Double('amount');
            $table->String('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitalpaymentrecords');
    }
}
