<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('d_name');
            $table->string('d_email',100)->unique();
            $table->Integer('d_age');
            $table -> float('d_salary');
            $table->string('d_address');
            $table->string('d_phone');
            $table->Integer('d_room_num')->unique();
            $table->String('speacilist');
            $table->string('d_password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
