<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuymedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buymedicines', function (Blueprint $table) {
            $table->increments('buy_id');
            $table->Integer('m_id')->references('m_id')->on('medicines')->onDelete('cascade')->onUpdate('cascade');
            $table->Integer('quantity');
            $table->Double('total');
            $table->String('buy_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buymedicines');
    }
}
