<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->increments('operation_id');
            $table->Integer('d_id')->references('id')->on('doctors')->onDelete('cascade')->onUpdate('cascade');
             $table->Integer('p_id')->references('id')->on('paients')->onDelete('cascade')->onUpdate('cascade');
             $table->String('o_date');
             $table->Integer('o_cost');
             $table->String('purpose');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
