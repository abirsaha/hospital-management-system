<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStuffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stuffs', function (Blueprint $table) {
            $table->increments('stuff_id');
            $table->string('s_name');
            $table->Integer('s_age');
            $table->string('s_duty_time');
            $table->Float('s_salary');
            $table->string('s_address');
            $table->string('s_email',100)->unique();
            $table->string('s_phone');
            $table->String('work_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stuffs');
    }
}
