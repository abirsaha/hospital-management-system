<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinepaymentrecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicinepaymentrecords', function (Blueprint $table) {
            $table->increments('mpay_id');
            $table->String('p_name');
            $table->String('p_phone');
            $table->Double('amount');
            $table->String('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicinepaymentrecords');
    }
}
