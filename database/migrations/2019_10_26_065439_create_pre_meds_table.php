<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreMedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_meds', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('pre_id')->references('pre_id')->on('prescriptions')->onDelete('cascade')->onUpdate('cascade');
            $table->Integer('med_id')->references('med_id')->on('medicines')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_meds');
    }
}
