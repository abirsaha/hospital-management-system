<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paients', function (Blueprint $table) {
            $table->increments('id');
             $table->string('p_name');
            $table->Integer('p_age');
            $table->string('p_email');
            $table->string('p_address');
            $table->string('p_phone',100)->unique();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paients');
    }
}
